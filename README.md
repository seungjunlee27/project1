# Car Sketch Tool #

### Objectives

* Easy sketching tool for kids who love car.
* Light sketching tool 
* [Demo video](https://youtu.be/vv6CeIJ0GVM)
	
### What it does
* Provides side, rear, front and diagonal view. You can draw each profile of a car separately using each perspective. 
* Provides mirror effect in front and rear view mode.
*  Draws basic sketch element, which are box, wheel, line and curve.
* Delete objects drawn or adjust their strokeweight
*  Pick a color from a palette and paint.
* Change thickness of the paint brush.
* Export the sketch in pdf.

### How to use
1) Choose the perspective among side/front/rear/diagonal
2) The skecth doesn't go away though you swap between different perspectives.
3) Press 'r' to reset your entire sketch.
4) In order to draw a box, press box icon. Press left button and drag to move the box. Press right button and drag to resize the box.
5) In order to draw a wheel, press wheel icon. Press left button and drag to move the box. Press right button and drag to resize the box. In front and rear view, wheels are shown as a rectangle.
6) In order to draw a line, press to set the starting point and drag to stretch.
7) In order to draw a curve, click four times to set 2 control points and 2 vortex. Text pops up to show you how many and which point to pick. After curve is drawn, you must press ctrl button to save it. Otherwise it would disappear.
8) Press delete button. Click over box and wheel to delete them. Click over dot to delete line and curve.
9) Press stroke button to 
10) Turn off box/wheel/line/curve button and press paint button to paint.
Click over palette to choose the color. Scroll up or down to change the thickness of your paint brush.
11) Press 's' to start recording your sketch. Press again to stop and once again to resume. Press 'q' to export your sketch screen in pdf file.
12) You can unview box by toggling view button.
13) Turn on or off dark mode as you like.





### Structure
#### interface
* interface drawableAndeditable
    

    function     | what it does   |  
    ---------:| :----- |
    void draw() | it draws | 
    void changethickness(float a)     |   it changes thickness to a |
    float thickness()     |     it returns thickness | 

* * *
#### classes
* abstract class Element implements drawableAndeditable

    function     | what it does   |  
    ---------:| :----- |
    void draw() | it draws | 
    void changethickness(float a)     |   it changes thickness to a |
    float thickness()     |     it returns thickness | 


* class BoxOrFrontRearWheel extends Element 

* class sideWheel extends Element 

* class DiagonalWheel extends Element
* * *

* abstract class Drawing implements drawableAndeditable
    function     | what it does   |  
    ---------:| :----- |
    boolean isMouseOn() | it tells if mouse is on the drawing | 
    void complete()     |  it turn complete==true which means the line or curve is finished  |
    boolean iscomplete()     |     it returns true if the drawing is complete, otherwise false | 

* class Line extends Drawing
* class Curve extends Drawing

#### UI
* ControlP5 buttons are each linked to UI buttons. 
* ControlP5 buttons are hidden behind png images of UI buttons.
* when cp5 button is pressed, it dose **togglemode()** which turns on or off the UI button linked to it.
* whether each **mode**(dark, view, box, wheel, line, curve, side, front,rear,diagonal,delete,stroke,paint) is on or off is written in Arraylist **modeonchecklist** in form of true or false.
* the state is constantly updated through **updatemodeonchecklist()**

