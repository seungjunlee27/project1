/*
	Project 1
	Name of Project: Car Sketch Tool
	Author: SeungJun Lee
	Date: May 2020
*/
import controlP5.*;
import processing.pdf.*;

boolean recording;
PGraphicsPDF pdf;
int pagecount = 0;

Sketch sketch  = new Sketch();
Factory factory = new Factory();
ArrayList<Boolean> modeonchecklist = new ArrayList<Boolean>();
ArrayList<Button> buttons = new ArrayList<Button>();

ArrayList<Line> lineNowWorking = new ArrayList<Line>();
ArrayList<Curve> curveNowWorking = new ArrayList<Curve>();

boolean workingOnDrawing = false;;
boolean workingOnResizing = false;
boolean mouseDragging = false;

color red = color(204,55,55);
color blue = color(55,115,204);
float h=0, s=0,v=0;
color paintcolor = color(h,s,v);
boolean painting = false;
float paintThickness = 30;

void setup()//
{
    size(2880,1800);
    colorMode(RGB);
    println(width);
    println(height);
    initiatecp5();
    initiateUI();
}

void draw() 
{
    colorMode(RGB);
    if(modeonchecklist.get(0)) background(0);
    else background(255);
    drawfixedUI();
    drawSketch();
    drawcp5s();
    drawUI();   

    if(modeonchecklist.get(12))
    {
        colorMode(HSB);
        for(int y=0; y < 256; y++)
        {            
            for(int x=0; x < 256; x++)
            {
                stroke(x,y,255);
                point(width*0.87+x,height*0.38+y);
            } 
        }    
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void mousePressed()
{
    if(mouseButton==LEFT&&mouseInRightRange())
    //유호범위 안에서 왼쪽 클릭
    {        
        if(!modeonchecklist.get(10))//삭제모드가 아닐 떄
        {        
                 
            if(modeonchecklist.get(2))//박스 그리기 시작
            {
                if(modeonchecklist.get(6) && !iIntendtoResize(6)) factory.createbox(6);
                else if(modeonchecklist.get(7) && !iIntendtoResize(7)) factory.createbox(7);
                else if(modeonchecklist.get(8) && !iIntendtoResize(8)) factory.createbox(8);
            }
            else if(modeonchecklist.get(3))//휠 그리기 시작
            {
                if(modeonchecklist.get(6) && !iIntendtoResize(6)) factory.createwheel(6);
                else if(modeonchecklist.get(7) && !iIntendtoResize(7)) factory.createwheel(7);
                else if(modeonchecklist.get(8) && !iIntendtoResize(8)) factory.createwheel(8);
                else if(modeonchecklist.get(9) && !iIntendtoResize(9)) factory.createDiagonalWheel();
            }
            else if(modeonchecklist.get(4))//직선 그리기 시작
            {
                workingOnDrawing = true;
                if(modeonchecklist.get(6)){factory.createline(6);lineNowWorking.get(0).setstartpoint();}
                else if(modeonchecklist.get(7)){factory.createline(7);lineNowWorking.get(0).setstartpoint();}
                else if(modeonchecklist.get(8)){factory.createline(8);lineNowWorking.get(0).setstartpoint();}
                else if(modeonchecklist.get(9)){factory.createline(9);lineNowWorking.get(0).setstartpoint();}
            }
            else if(modeonchecklist.get(5))//커브 그리기 시작
            {
                workingOnDrawing = true;                    
                if(modeonchecklist.get(6)){factory.createcurve(6);curveNowWorking.get(0).addpoint();}
                else if(modeonchecklist.get(7)){factory.createcurve(7);curveNowWorking.get(0).addpoint();}
                else if(modeonchecklist.get(8)){factory.createcurve(8);curveNowWorking.get(0).addpoint();}
                else if(modeonchecklist.get(9)){factory.createcurve(9);curveNowWorking.get(0).addpoint();}
            }
            else if(modeonchecklist.get(12) && painting==false)
            {
                if(mouseX>=width*0.87&&mouseX<=width*0.87+255&&mouseY>=height*0.38 && mouseY<=height*0.38+255) 
                {
                    paintcolor = get(mouseX, mouseY);
                    println(paintcolor);
                }
            } 
        }
        else//삭제모드일 때
        {
            if(modeonchecklist.get(6)) delete(6);
            else if(modeonchecklist.get(7)) delete(7);
            else if(modeonchecklist.get(8)) delete(8);
            else if(modeonchecklist.get(9)) delete(9);
        }
    }
}


void mouseDragged() 
{
    mouseDragging = true;
    if(mouseButton==LEFT)
    {
        if(!workingOnDrawing)
        {         
            if(modeonchecklist.get(12))
            {
                if(mousePressed && !(mouseX>=width*0.87&&mouseX<=width*0.87+255&&mouseY>=height*0.38 && mouseY<=height*0.38+255) )
                {
                    PaintParticle pp = new PaintParticle(mouseX, mouseY,paintThickness,paintcolor);
                    if(modeonchecklist.get(6)) sideprofileD.add(pp);
                    else if(modeonchecklist.get(7)) frontprofileD.add(pp);
                    else if(modeonchecklist.get(8)) rearprofileD.add(pp);
                    else if(modeonchecklist.get(9)) diagonalprofileD.add(pp);  
                }
            }
            else
            {            
                if(modeonchecklist.get(6)) relocateElement(sideprofileE);
                else if(modeonchecklist.get(7)||modeonchecklist.get(8)) relocateElement(frontrearprofileE); 
                else if(modeonchecklist.get(9)) relocateElement(diagonalprofileE); 
            } 
                
        }
        else if(workingOnDrawing&&modeonchecklist.get(4)&&!lineNowWorking.isEmpty())
        {
            lineNowWorking.get(0).setendpoint();
        }
    }
    else if(mouseButton==RIGHT )//resize Element
    {
        if(!workingOnDrawing)
        {
            if(modeonchecklist.get(6)) resizeElement(sideprofileE);
            else if(modeonchecklist.get(7)||modeonchecklist.get(8)) resizeElement(frontrearprofileE);  
            else if(modeonchecklist.get(9)) resizeElement(diagonalprofileE);   
        }
    }    
}

void mouseReleased() 
{    
    lineNowWorking.clear();
    workingOnDrawing = false;
    workingOnResizing = false;
    mouseDragging = false;
}

void keyPressed() 
{
    if(key == 'r') 
    {
        sketch.reset();
    }
    if(key == CODED)
    {
        if(keyCode == 16)
        {
        }
        
        if(keyCode == CONTROL)//커브 확정하기//버그 고치기
        {
            if(!curveNowWorking.isEmpty()) 
            {curveNowWorking.get(0).complete();}
            workingOnDrawing=false; 
            curveNowWorking.clear();
            println("ok");
            //paintNowWorking.clear();
        }
    }
    if (key == 's') {
        pdf = (PGraphicsPDF) createGraphics(width, height, PDF, "Sketch"+pagecount+".pdf");
        if (recording) 
        {
            endRecord();
            println("Recording stopped.");
            recording = false;
        } 
        else 
        {
            pagecount+=1;
            beginRecord(pdf);
            println("Recording started.");
            recording = true;
        }
    } 
    else if (key == 'q') 
    {
        if (recording) 
        {
            endRecord();
            println("exported");
        }
    }  
}

void mouseWheel(MouseEvent event) 
{
    float e = event.getCount();
    if(modeonchecklist.get(11)) 
    {
        if(modeonchecklist.get(6)) changethicknessLC(6,e);
        else if(modeonchecklist.get(7)) changethicknessLC(7,e);
        else if(modeonchecklist.get(8)) changethicknessLC(8,e);
        else if(modeonchecklist.get(9)) changethicknessLC(9,e);
    }    
    if(modeonchecklist.get(12))
    {
        paintThickness+=e;
    }
    println(e);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class Sketch
{
	void reset()
	{
        modeonchecklist.clear();

        sideprofileE.clear();sideprofileD.clear();
        frontrearprofileE.clear();
        frontprofileD.clear();
        rearprofileD.clear();
        diagonalprofileE.clear();diagonalprofileD.clear();
        
        initiateUI();
	}
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class Factory
{
	void createbox(int a)
	{
		BoxOrFrontRearWheel box = new BoxOrFrontRearWheel(0,width/1440*150,height/900*100,width/1440*50,height/900*50);  
        if(a==6) sideprofileE.add(box);
		else if(a==7||a==8) frontrearprofileE.add(box);
	}

	void createwheel(int b)
	{
		SideWheel side = new SideWheel(width/1440*150,height/900*300,width/1440*50,height/900*50);
        BoxOrFrontRearWheel frontrear = new BoxOrFrontRearWheel(1, width/1440*175, height/900*240, width/1440*50,height/900*50);

		if(b==6) sideprofileE.add(side);
		else if(b==7||b==8) frontrearprofileE.add(frontrear);
	}

	void createline(int c)
	{
        if(lineNowWorking.isEmpty())
        {        
            Line l = new Line();
            lineNowWorking.add(l);
            if(c==6) sideprofileD.add(l);
            else if(c==7) frontprofileD.add(l);  
            else if(c==8) rearprofileD.add(l);  
            else if(c==9) diagonalprofileD.add(l);
        }
        else
        {
            if(!lineNowWorking.get(0).iscomplete())
            {
                Line l = new Line();
                if(c==6) {sideprofileD.remove(lineNowWorking.get(0));sideprofileD.add(l);}
                else if(c==7) {frontprofileD.remove(lineNowWorking.get(0));frontprofileD.add(l);}
                else if(c==8) {rearprofileD.remove(lineNowWorking.get(0));rearprofileD.add(l);}
                else{diagonalprofileD.remove(lineNowWorking.get(0));diagonalprofileD.add(l);} 
                lineNowWorking.clear();
                lineNowWorking.add(l);                
            }
        }
	}

	void createcurve(int d)
	{
        if(curveNowWorking.isEmpty())
        {           
            Curve c = new Curve();
            curveNowWorking.add(c);
            if(d==6) sideprofileD.add(c);
		    else if(d==7) frontprofileD.add(c);  
            else if(d==8) rearprofileD.add(c);
            else diagonalprofileD.add(c); 
        }
        else
        {
            if(curveNowWorking.get(0).pointSoFar()==4 && !(curveNowWorking.get(0).iscomplete()))
            {
                Curve c = new Curve();
                
                if(d==6) {sideprofileD.remove(curveNowWorking.get(0));sideprofileD.add(c);}
                else if(d==7) {frontprofileD.remove(curveNowWorking.get(0));frontprofileD.add(c);}
                else if(d==8) {rearprofileD.remove(curveNowWorking.get(0));rearprofileD.add(c);}
                else{diagonalprofileD.remove(curveNowWorking.get(0));diagonalprofileD.add(c);} 
                curveNowWorking.clear();
                curveNowWorking.add(c);
            }
        }
	}

    void createDiagonalWheel()
    {
		DiagonalWheel diagonal = new DiagonalWheel(width/1440*300,height/900*400,width/1440*50,height/900*50);
        diagonalprofileE.add(diagonal);
    }

}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
boolean mouseInRightRange()
{
    if ((mouseX>width/20&&mouseX<width&&mouseY>0&&mouseY<height)&&!(mouseX>width/2880*2539&&mouseX<width/2880*2727&&mouseY>height/1800*160&&mouseY<height/1800*410)) return true;
    else return false;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ArrayList<Element>sideprofileE = new ArrayList<Element>(); 
ArrayList<Drawing>sideprofileD = new ArrayList<Drawing>(); 

ArrayList<Element>frontrearprofileE = new ArrayList<Element>(); 
ArrayList<Drawing>frontprofileD = new ArrayList<Drawing>();
ArrayList<Drawing>rearprofileD = new ArrayList<Drawing>();

ArrayList<Element>diagonalprofileE = new ArrayList<Element>(); 
ArrayList<Drawing>diagonalprofileD = new ArrayList<Drawing>();
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void drawElement(ArrayList<Element> a)
{
    if(modeonchecklist.get(1))
    {        
        if(a.isEmpty()) return;
        else{for(Element e: a){e.draw();}}
    } 

    else if(!modeonchecklist.get(1))
    {
        if(a.isEmpty()) return;
        else
        {
            for(Element e: a)
            {   
                if(e.getelementnumber()==1 || e.getelementnumber()==2)
                {e.draw();}
            }
        }
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void drawDrawing(ArrayList<Drawing> a)
{
    if(a.isEmpty()) return;
    else
    {
        for(Drawing d: a)
        {
            d.draw();
        }
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void delete(int a)
{
    if(a==6)
    {
        for(Element e: sideprofileE){if(e.isMouseOn()){sideprofileE.remove(e);break;}}
        for(Drawing d: sideprofileD){if(d.isMouseOn()){sideprofileD.remove(d);break;}}
    }
    else if(a==7)
    {
        for(Element e: frontrearprofileE){if(e.isMouseOn()){frontrearprofileE.remove(e);break;}}
        for(Drawing d: frontprofileD){if(d.isMouseOn()){frontprofileD.remove(d);break;}}
    }
    else if(a==8)
    {
        for(Element e: frontrearprofileE){if(e.isMouseOn()){frontrearprofileE.remove(e);break;}}
        for(Drawing d: rearprofileD){if(d.isMouseOn()){rearprofileD.remove(d);break;}}
    }   
    else if(a==9)
    {
        for(Element e: diagonalprofileE){if (e.isMouseOn()){diagonalprofileE.remove(e);break;}}
        for(Drawing d: diagonalprofileD){if(d.isMouseOn()){diagonalprofileD.remove(d);break;}}
    }      
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void changethicknessLC(int a, float b)
{
    if(a==6)
    {
        for(Element e: sideprofileE){if(e.isMouseOn()){if(e.thickness()+b>0){e.changethickness(b);}}break;}
        for(Drawing d: sideprofileD){if(d.isMouseOn()){if(d.thickness()+b>0){d.changethickness(b);}}break;}
    }
    else if(a==7)
    {
        for(Element e: frontrearprofileE){if(e.isMouseOn()){if(e.thickness()+b>0){e.changethickness(b);}}break;}
        for(Drawing d: frontprofileD){if(d.isMouseOn()){if(d.thickness()+b>0){d.changethickness(b);}}break;}
    }
    else if(a==8)
    {
        for(Element e: frontrearprofileE){if(e.isMouseOn()){if(e.thickness()+b>0){e.changethickness(b);}}break;}
        for(Drawing d: rearprofileD){if(d.isMouseOn()){if(d.thickness()+b>0){d.changethickness(b);}}break;}
    }   
    else if(a==9)
    {
        for(Element e: diagonalprofileE){if(e.isMouseOn()){if(e.thickness()+b>0){e.changethickness(b);}}break;}
        for(Drawing d: diagonalprofileD){if(d.isMouseOn()){if(d.thickness()+b>0){d.changethickness(b);}}break;}
    }    
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void drawSketch()
{
    if(modeonchecklist.get(6)==true){drawDrawing(sideprofileD);drawElement(sideprofileE);}
    else if(modeonchecklist.get(7)==true)
    {
        drawDrawing(frontprofileD);
        drawElement(frontrearprofileE);
        pushMatrix();
        translate(width,0);
        scale(-1,1);
        drawElement(frontrearprofileE);
        drawDrawing(frontprofileD);
        popMatrix();
    }
    else if(modeonchecklist.get(8)==true)
    {   
        drawDrawing(rearprofileD);
        drawElement(frontrearprofileE);
        pushMatrix();
        translate(width,0);
        scale(-1,1);
        drawElement(frontrearprofileE);
        drawDrawing(rearprofileD);
        popMatrix();       
    }
    else if(modeonchecklist.get(9)==true){drawDrawing(diagonalprofileD);drawElement(diagonalprofileE);}
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void relocateElement(ArrayList<Element> arr)
{    
    int[] numbers = new int[1];
    boolean bool = false;

    for (int i = 0; i < arr.size(); ++i) 
    {
        Element e=arr.get(i);
        if(e.isMouseOn()) {numbers[0]=i;bool = true; break;}
    }

    if(bool)
    {   
        arr.get(numbers[0]).correctlocation();arr.get(numbers[0]).relocate();
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void resizeElement(ArrayList<Element> arr)
{
    int[] numbers = new int[1];
    boolean bool = false;

    for (int i = 0; i < arr.size(); ++i) 
    {
        Element e=arr.get(i);
        if(e.isMouseOn()) {workingOnResizing=true;bool=true;numbers[0]=i;break;}
    }

    if(bool)arr.get(numbers[0]).resize();
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
boolean iIntendtoResize(int a)
{
    boolean intendtoresize = false;

    if(a==6)
    {
        if(!(sideprofileE.isEmpty()))
        {
            for(Element e: sideprofileE)
            {
                if(e.isMouseOn()) {intendtoresize = true; break;}
            }
        }
    }
    else if(a==7||a==8)
    {
        if(!(frontrearprofileE.isEmpty()))
        {
            for(Element e: frontrearprofileE)
            {
                if(e.isMouseOn()) {intendtoresize = true; break;} 
            }
        }
    }
    if(a==9)
    {
        if(!(diagonalprofileE.isEmpty()))
        {
            for(Element e: diagonalprofileE)
            {
                if(e.isMouseOn()) {intendtoresize = true; break;}
            }
        }
    }   
    return intendtoresize;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
abstract class Element implements drawableAndeditable
{
	protected int i;
    protected float x; float y;
	protected float w; float h;

	Element(int i, float x,float y, float w, float h)
	{
		this.i = i;
        this.x = x; this.y = y;
		this.w = w; this.h = h;
	}
    abstract boolean isMouseOn();
	abstract void relocate();
    abstract void correctlocation();
	abstract void resize();
    int getelementnumber(){return this.i;}  
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class BoxOrFrontRearWheel extends Element  //every object CENTER mode
{	
	private float thickness=5;
    private float x; float y;
	private float w; float h;
    private int num;
	BoxOrFrontRearWheel(int num, float x, float y, float w, float h)
	{
		super(num,x,y,w,h);
        this.num = num;
		this.x = x; this.y = y;
		this.w = w; this.h = h;
	}

    boolean isMouseOn()
    {
        if((mouseX>=this.x&&mouseX<=this.x+w) && (mouseY>=this.y&&mouseY<=this.y+h)) return true;
        else return false;
    }

	void relocate()
	{
        float dx = mouseX - pmouseX;float dy = mouseY - pmouseY;
        this.x+=dx; this.y+=dy;       
	}

    void correctlocation()
    {
        if(modeonchecklist.get(6)==true)
        {
            if(this.x<=width/20) this.x = width/20;
            else if(this.x+this.w>=width) this.x = width-this.w;
            
            if(this.y<=0) this.y =0;
            else if(this.y+this.h>=height) this.y = height-this.h; 
        }
        else if(modeonchecklist.get(7)||modeonchecklist.get(8))
        {
            if(this.x<=width/20) this.x = width/20;
            else if(this.x+this.w>=width/2) this.x = width/2-this.w;
            
            if(this.y<=0) this.y =0;
            else if(this.y>=height) this.y = y-this.h;                 
        }
    }

	void resize()
	{
        float dx = mouseX - pmouseX;float dy = mouseY - pmouseY;
        float xLimit = this.x+this.w+dx;
        float yLimit = this.y+this.h+dy;

        if(modeonchecklist.get(6) && (xLimit>=width/20&&xLimit<width) && (yLimit>=0&&yLimit<height))
        {
            this.w+=dx; this.h+=dy; 
        }
        else if((modeonchecklist.get(7)||modeonchecklist.get(8))&&(xLimit>=width/20&&xLimit<=width/2)&&(yLimit>=0&&yLimit<height))
        {
            this.w+=dx; this.h+=dy; 
        }
	}

	void draw()
	{
		if(this.num==0)
        {        
            rectMode(CORNER);
            if(modeonchecklist.get(10)) stroke(red);
            else if(modeonchecklist.get(11)) stroke(blue);
            else if(modeonchecklist.get(0)) stroke(230);
            else stroke(0);
            strokeWeight(thickness);
            noFill();
            rect(this.x,this.y,this.w,this.h);
        }

        else
        {        
            rectMode(CORNER);
            if(modeonchecklist.get(10)) stroke(red);
            else if(modeonchecklist.get(11)) stroke(blue);
            else if(modeonchecklist.get(0)) stroke(255);
            else stroke(144);
            strokeWeight(thickness);
            noFill();
            rect(this.x,this.y,this.w,this.h);
        }       
	}
    
    void changethickness(float a){this.thickness += a;}
    float thickness(){return this.thickness;}
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class SideWheel extends Element 
{
	private float thickness=5;
    private float x; float y;
	private float r1; float r2; 
	SideWheel(float x, float y, float r1, float r2)
	{
		super(2,x,y,r1,r2);
		this.x=x; this.y=y;
		this.r1 = r1; this.r2=r2;
	}
	
    boolean isMouseOn()
    {
        float d = dist(this.x,this.y,mouseX,mouseY);
        if(d>=0 && d<=this.r1) return true;
        else return false;
    }

	void relocate()
	{
        float dx = mouseX - pmouseX;float dy = mouseY - pmouseY;
        this.x+=dx; this.y+=dy;                       
	}

    void correctlocation()
    {
        if(this.x<=width/20+this.r1/2) this.x = width/20+this.r1/2;
        else if(this.x>=width-this.r1/2) this.x = width-this.r1/2;
           
        if(this.y<=this.r1/2) this.y =this.r1/2;
        else if(this.y>=height-this.r1/2) this.y = height-this.r1/2;  
    }

	void resize()
	{        
        float dx = mouseX - pmouseX;
        float xLimit1 = this.x-this.r1/2-dx;
        float xLimit2 = this.x+this.r1/2-+dx;
        float yLimit1 = this.y-this.r1/2-dx;
        float yLimit2 = this.y+this.r1/2+dx;
        
        if(modeonchecklist.get(6) && (xLimit1>=width/20&&xLimit2<width) && (yLimit1>=0&&yLimit2<height))
        {
            this.r1+=dx; this.r2+=dx; 
        }
        else if((modeonchecklist.get(7)||modeonchecklist.get(8))&&(xLimit1>=width/20 && xLimit2<=width/2)&&(yLimit1>=0&&yLimit2<height))
        {
            this.r1+=dx; this.r2+=dx; 
        }
	}

    void draw()
	{
        ellipseMode(CENTER);
        if(modeonchecklist.get(10)) stroke(red);
        else if(modeonchecklist.get(11)) stroke(blue);
        else if(modeonchecklist.get(0)) stroke(255);
        else stroke(0);
        strokeWeight(thickness);
        noFill();
        ellipse(this.x,this.y,this.r1,this.r2);
	}
    
    void changethickness(float a){this.thickness += a;}
    float thickness(){return this.thickness;}
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class DiagonalWheel extends SideWheel implements drawableAndeditable
{   
	private float thickness=5;
    private float x; float y;
	private float r1; float r2; 
    private float xI=0; float yI=0;

	DiagonalWheel(float x, float y, float r1, float r2)
	{
		super(x,y,r1,r2);
		this.x=x; this.y=y;
		this.r1 = r1; this.r2=r2;
	}
	
    boolean isMouseOn()
    {
        float dX = abs(this.x-mouseX);
        float dY = abs(this.y-mouseY);
        if(dX<=this.r1&&dY<=this.r2) return true;
        else return false;
    }

	void relocate()
	{
        float dx = mouseX - pmouseX;float dy = mouseY - pmouseY;
        this.x+=dx; this.y+=dy;                       
	}

    void correctlocation()
    {
        if(this.x<=width/20+this.r1/2) this.x = width/20+this.r1/2;
        else if(this.x>=width-this.r1/2) this.x = width-this.r1/2;
           
        if(this.y<=this.r1/2) this.y =this.r1/2;
        else if(this.y>=height-this.r1/2) this.y = height-this.r1/2;  
    }

	void resize()
	{        
        float dx = mouseX - pmouseX;
        float dy = mouseY - pmouseY;

        float xLimit1 = this.x-this.r1-dx;
        float xLimit2 = this.x+this.r1+dx;
        float yLimit1 = this.y-this.r2-dy;
        float yLimit2 = this.y+this.r2+dy;
        
        if((xLimit1>=width/20&&xLimit2<width) && (yLimit1>=0&&yLimit2<height))
        {
            this.r1+=dx; this.r2+=dy; 
        }
	}

    void draw()
	{
        ellipseMode(CENTER);
        if(modeonchecklist.get(10)) stroke(red);
        else if(modeonchecklist.get(11)) stroke(blue);
        else if(modeonchecklist.get(0)) stroke(255);
        else stroke(0);
        strokeWeight(thickness);
        noFill();
        ellipse(this.x,this.y,this.r1*2,this.r2*2);
	}
    
    void changethickness(float a){this.thickness += a;}
    float thickness(){return this.thickness;}   
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
interface drawableAndeditable
{
    void draw();
    void changethickness(float a);
    float thickness();
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
abstract class Drawing implements drawableAndeditable
{
    Drawing(){}
    abstract boolean isMouseOn();
    abstract void complete();
    abstract boolean iscomplete();
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class Line extends Drawing 
{
	private boolean complete = false;
    private float x1; float y1;
    private float x2; float y2;
    private ArrayList<Float> linePoints = new ArrayList<Float>();
    float thickness = 5;

    Line(){}
	Line(float x1, float y1, float x2, float y2){}
    void setstartpoint()
    {
        this.x1 = mouseX; this.y1 = mouseY;
    }
    void setendpoint()
    {
        this.x2 = mouseX; this.y2 = mouseY;
        this.complete = true;
    }
	void draw()
	{
        if(modeonchecklist.get(10)) stroke(red);
        else if(modeonchecklist.get(11)) stroke(blue);
        else if(modeonchecklist.get(0)) stroke(255);
        else stroke(0);

        strokeWeight(thickness);
        if(this.x2!=0 && this.y2!=0)
        {line(this.x1, this.y1, this.x2, this.y2);}
        
        if(modeonchecklist.get(10)||modeonchecklist.get(11))
        {
            ellipseMode(CENTER);
            if(modeonchecklist.get(10))stroke(red);
            else stroke(blue);
            strokeWeight(thickness);
            if(modeonchecklist.get(10))fill(red);
            else fill(blue);
            ellipse(this.x1, this.y1, 50,50);
        }
	}
    boolean isMouseOn()
    {
        float d = dist(mouseX, mouseY, this.x1, this.y1);
        if(d<=25) return true;
        else return false;
    }

    void complete(){this.complete = true;}
    boolean iscomplete(){return this.complete;}
    
    void changethickness(float a){this.thickness += a;}
    float thickness(){return this.thickness;}
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class Curve extends Drawing 
{
    private ArrayList<Float> curvepoint = new ArrayList<Float>();
    private int pointcount = 0;
    private boolean complete = false;
    private float thickness = 5;
    PFont font = createFont("NanumSquareB.ttf",20);
    
    Curve(){}

	void draw()
	{
        if(pointcount>0 && pointcount<4)
        {
            ellipseMode(CENTER);
            noFill();
            if(modeonchecklist.get(0)) stroke(255);
            else stroke(0);
            for (int i = 1; i < pointcount+1; ++i) 
            {
                ellipse(curvepoint.get(2*i-2),curvepoint.get(2*i-1),20,20);
            } 

            if(pointcount>=1)
            {
                textAlign(CENTER);fill(red);textSize(20);textFont(font);
                if(!modeonchecklist.get(7)&&!modeonchecklist.get(8))text("control point 1 of 2", curvepoint.get(0), curvepoint.get(1)-30);
                if(pointcount>=2 && !modeonchecklist.get(7)&&!modeonchecklist.get(8))
                {
                    textAlign(CENTER);fill(red);textSize(20);textFont(font);
                    text("curve point 1 of 2", curvepoint.get(2), curvepoint.get(3)-30);                   
                }
                if(pointcount==3 && !modeonchecklist.get(7) && !modeonchecklist.get(8))
                {
                    text("curve point 2 of 2", curvepoint.get(4), curvepoint.get(5)-30);
                    text("choose control point 2 of 2", width/2, height/2);
                }
            }
        }
        else if(pointcount==4)
        {       
            noFill();
            if(modeonchecklist.get(10)) stroke(red);
            else if(modeonchecklist.get(11)) stroke(blue);
            else if(modeonchecklist.get(0)) stroke(255);
            else stroke(0);
            strokeWeight(thickness);
            curve(curvepoint.get(0), curvepoint.get(1), curvepoint.get(2), curvepoint.get(3), curvepoint.get(4), curvepoint.get(5), curvepoint.get(6), curvepoint.get(7));
        
            if(modeonchecklist.get(10)||modeonchecklist.get(11))
            {            
                ellipseMode(CENTER);
                if(modeonchecklist.get(10)) stroke(red);
                else stroke(blue);
                strokeWeight(thickness);
                if(modeonchecklist.get(10)) fill(red);
                else fill(blue);
                ellipse(curvepoint.get(2), curvepoint.get(3), 50,50);
            }
        }
	}

    void addpoint()
    {
        if(this.pointcount<5)
        {        
            curvepoint.add(float(mouseX)); 
            curvepoint.add(float(mouseY));
            this.pointcount+=1;
        }
    }

    int pointSoFar(){return this.pointcount;}
    boolean isMouseOn()
    {
        float d = dist(mouseX, mouseY, curvepoint.get(2), curvepoint.get(3));
        if(d<=25) return true;
        else return false;
    }
    void complete(){this.complete = true;}
    boolean iscomplete(){return this.complete;}
    
    void changethickness(float a){this.thickness += a;}
    float thickness(){return this.thickness;}
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class PaintParticle extends Drawing
{
    int x; int y; float r; color c;
    PaintParticle(int x,int y,float r, color c)
    {
        this.x=x; this.y=y;this.r=r;this.c=c;
    }

    void draw()
    {
        fill(c);
        circle(x,y,r);
    }

    boolean isMouseOn(){return true;}
    void complete(){}
    boolean iscomplete(){return true;}
    void changethickness(float a){this.r=a;}
    float thickness(){return 0;}
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void initiateUI()//버튼을 만들고 모드체크리스트에 모드를 추가, 최초 실행할 때만 작동
{   
    boolean drbmode = false; boolean mvmode = true;
    boolean dbbmode = false; boolean dwbmode = false;boolean dlbmode = false; boolean dcbmode = false;
    boolean svbmode = true; boolean fvbmode = false;boolean dvbmode = false; boolean rvbmode = false;
    boolean dbmode = false; boolean stbmode = false;
    boolean pbmode = false;
    
    ModeDarkButton drb = new ModeDarkButton(drbmode); buttons.add(drb);
    ModeViewButton mv = new ModeViewButton(mvmode); buttons.add(mv);
    
    DrawBoxButton dbb = new DrawBoxButton(dbbmode); buttons.add(dbb);
    DrawWheelButton dwb = new DrawWheelButton(dwbmode); buttons.add(dwb);
    DrawLineButton dlb = new DrawLineButton(dlbmode); buttons.add(dlb);
    DrawCurveButton dcb = new DrawCurveButton(dcbmode); buttons.add(dcb);
    
    SideViewButton svb = new SideViewButton(svbmode); buttons.add(svb);
    FrontViewButton fvb = new FrontViewButton(fvbmode); buttons.add(fvb);
    RearViewButton rvb = new RearViewButton(rvbmode); buttons.add(rvb);
    DiagonalViewButton dvb = new DiagonalViewButton(dvbmode); buttons.add(dvb);

    DeleteButton db = new DeleteButton(dbmode); buttons.add(db);
    StrokeButton stb = new StrokeButton(stbmode); buttons.add(stb);

    PaintButton pb = new PaintButton(pbmode); buttons.add(pb);

    for(Button b: buttons)
    {
        modeonchecklist.add(b.buttonIsOn());
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void drawfixedUI()//고정된 줄 그리기, 최초 실행할 때만 작동
{
    rectMode(CORNER);
    noStroke();
    fill(233,233,233);
    rect(0, 0, width/25, height);
    if(modeonchecklist.get(7)||modeonchecklist.get(8))
    {
        if(modeonchecklist.get(0))stroke(255);
        else stroke(0);

        strokeWeight(5);
        line(width/2, height/70, width/2, height*9/10);
        PImage mirrorD = loadImage("MirrorD.png");
        PImage mirrorL = loadImage("MirrorL.png");
        imageMode(CENTER);
        
        if(modeonchecklist.get(0))image(mirrorD, width/2, height*19.3/20,width/1440*mirrorD.width/3, height/900*mirrorD.height/3);
        else image(mirrorL, width/2, height*19.3/20,width/1440*mirrorL.width/3, height/900*mirrorL.height/3);
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void drawUI()//UI를 그림
{
    for (int i = 0; i < buttons.size(); ++i) 
    {
        imageMode(CORNER);
        Button but = buttons.get(i);
        PImage img = but.getImage();
        float x = width/1440*img.width/3;
        float y = height/900*img.height/3;
        switch(i)
        {   
            case 0:image(img, width/1440*13, height/900*86,x,y); break;
            case 1:image(img, width/1440*13, height/900*175,x,y); break;
            
            case 2:image(img, width/1440*11, height/900*230,x,y); break;
            case 3:image(img, width/1440*18, height/900*362,x,y); break;
            case 4:image(img, width/1440*20, height/900*442,x,y); break;
            case 5:image(img, width/1440*20, height/900*531,x,y); break;
            
            case 6:image(img, width-2*x, height/900*87,x,y); break;
            case 7:image(img, width-2*x, height/900*118,x,y); break;
            case 8:image(img, width-2*x, height/900*149,x,y); break;
            case 9:image(img, width-2*x, height/900*180,x,y); break;

            case 10: image(img, width/1440*12, height/900*610,x,y); break;
            case 11: image(img, width/1440*19, height/900*750,x,y); break;

            case 12: image(img, width-4.1*x, height/900*240,x,y); break;
        }
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void togglemode(Button b)//켜져있으면 끄고 꺼져있으면 조건에 맞춰서 끈 다음 모드 체크리스트를 업데이트한다.
{
    if(b.buttonIsOn()) 
    {
        b.turnoff();
        updatemodeonchecklist();
    }
	else
    {
        int num = b.buttonnumber();
        if(num==0||num==1)
        {
            b.turnon(); 
            updatemodeonchecklist();
        }
        else if(num==2||num==3||num==4||num==5)
        {
            if(!(modeonchecklist.get(10)||modeonchecklist.get(11)||modeonchecklist.get(12)))
            {
                buttons.get(2).turnoff();buttons.get(3).turnoff();buttons.get(4).turnoff();buttons.get(5).turnoff();
                b.turnon();
                updatemodeonchecklist();
            }
        } 
        else if(num==6||num==7||num==8||num==9)
        {
            buttons.get(6).turnoff();buttons.get(7).turnoff();buttons.get(8).turnoff();buttons.get(9).turnoff();
            b.turnon();
            updatemodeonchecklist();
        }
        else if(num==10||num==11)
        {
            buttons.get(10).turnoff();buttons.get(11).turnoff();
            b.turnon();
            updatemodeonchecklist();
        }
        else if(num==12)
        {
            buttons.get(2).turnoff();buttons.get(3).turnoff();buttons.get(4).turnoff();buttons.get(5).turnoff();
            buttons.get(10).turnoff();buttons.get(11).turnoff();
            
            b.turnon();
            updatemodeonchecklist();
        }
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void updatemodeonchecklist()//모드 체크리스트를 업데이트
{
    modeonchecklist.clear();
    for(Button b: buttons)
    {
        modeonchecklist.add(b.buttonIsOn());
    } 
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
abstract class Button
{
	protected PImage img;protected PImage [] imgs = new PImage[2];
    protected boolean isOn;
    
    Button(boolean isOn)
    {this.isOn = isOn;}

	PImage getImage()
	{
        if(isOn) img = imgs[0]; else img = imgs[1];
        return img; 
    }
    
    abstract int buttonnumber();
    boolean buttonIsOn() 
    {return isOn;}
    void turnoff() {this.isOn = false;}
    void turnon() {this. isOn = true;}
}
//0번 버튼
class ModeDarkButton extends Button
{
    boolean darkon;
    ModeDarkButton(boolean darkon)
    {
        super(darkon);
        imgs[0]= loadImage("DarkOn.png");
        imgs[1]= loadImage("DarkOff.png");
    }

    int buttonnumber(){return 0;}
}
//1번 버튼
class ModeViewButton extends Button
{
    boolean viewon;
    ModeViewButton(boolean viewon)
    {
        super(viewon);
        imgs[0]= loadImage("ViewOn.png");
        imgs[1]= loadImage("ViewOff.png");
    }
    int buttonnumber(){return 1;}
} 
//2번 버튼
class DrawBoxButton extends Button
{
    boolean drawboxon;
    DrawBoxButton(boolean drawboxon)
    {
        super(drawboxon);
        imgs[0]= loadImage("BoxOn.png");
        imgs[1]= loadImage("BoxOff.png");
    }
    int buttonnumber(){return 2;}
}
//3번 버튼
class DrawWheelButton extends Button
{
    boolean drawwheelon;
    DrawWheelButton(boolean drawwheelon)
    {
        super(drawwheelon);
        imgs[0]= loadImage("WheelOn.png");
        imgs[1]= loadImage("WheelOff.png");
    }
    int buttonnumber(){return 3;}
}
//4번 버튼
class DrawLineButton extends Button
{
    boolean drawlineon;
    DrawLineButton(boolean drawlineon)
    {
        super(drawlineon);
        imgs[0]= loadImage("LineOn.png");
        imgs[1]= loadImage("LineOff.png");
    }
    int buttonnumber(){return 4;}
}
//5번 버튼
class DrawCurveButton extends Button
{
    boolean drawcurveon;
    DrawCurveButton(boolean drawcurveon)
    {
        super(drawcurveon);
        imgs[0]= loadImage("CurveOn.png");
        imgs[1]= loadImage("CurveOff.png");
    }
    int buttonnumber(){return 5;}
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
abstract class AngleViewButton extends Button
{
    AngleViewButton(boolean angleon)
    {
        super(angleon);
    }
}
//6번 버튼
class SideViewButton extends AngleViewButton
{
    boolean sideon;
    SideViewButton(boolean sideon)
    {
        super(sideon);
        imgs[0]= loadImage("SideOn.png");
        imgs[1]= loadImage("SideOff.png");
    }
    int buttonnumber(){return 6;}
}
//7번 버튼
class FrontViewButton extends AngleViewButton
{
    boolean fronton;
    FrontViewButton(boolean fronton)
    {
        super(fronton);
        imgs[0]= loadImage("FrontOn.png");
        imgs[1]= loadImage("FrontOff.png");
    }
    int buttonnumber(){return 7;}
}
//8번 버튼
class RearViewButton extends AngleViewButton
{
    boolean rearon;
    RearViewButton(boolean rearon)
    {
        super(rearon);
        imgs[0]= loadImage("RearOn.png");
        imgs[1]= loadImage("RearOff.png");
    }
    int buttonnumber(){return 8;}
}
//9번 버튼
class DiagonalViewButton extends AngleViewButton
{
    boolean diagonalon;
    DiagonalViewButton(boolean diagonalon)
    {
        super(diagonalon);
        imgs[0]= loadImage("DiagonalOn.png");
        imgs[1]= loadImage("DiagonalOff.png");
    }
    int buttonnumber(){return 9;}
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//10번 버튼
class DeleteButton extends Button
{
    boolean deleteon;
    DeleteButton(boolean deleteon)
    {
        super(deleteon);
        imgs[0]= loadImage("DeleteOn.png");
        imgs[1]= loadImage("DeleteOff.png");
    }
    int buttonnumber(){return 10;}
}
//11번 버튼
class StrokeButton extends Button
{
    boolean strokeon;
    StrokeButton(boolean strokeon)
    {
        super(strokeon);
        imgs[0]= loadImage("StrokeOn.png");
        imgs[1]= loadImage("StrokeOff.png");
    }
    int buttonnumber(){return 11;}
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//12번 버튼
class PaintButton extends Button
{
    boolean painton;
    PaintButton(boolean painton)
    {
        super(painton);
        imgs[0]= loadImage("PaintOn.png");
        imgs[1]= loadImage("PaintOff.png");
    }
    int buttonnumber(){return 12;}
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ControlP5 dark; ControlP5 view;
ControlP5 box; ControlP5 wheel; ControlP5 drawnline; ControlP5 drawncurve;
ControlP5 side; ControlP5 front; ControlP5 rear; ControlP5 diagonal;
ControlP5 bin; ControlP5 thickness;
ControlP5 paint;
ControlP5 [] cp5s = new ControlP5[13];

void initiatecp5()
{
    dark = new ControlP5(this);
    dark.addButton("dark").setValue(0).setPosition(width/1440*14,height/900*125).setSize(width/1440*40,height/900*16);
    dark.setAutoDraw(false);
    cp5s[0] = dark;

    view = new ControlP5(this);
    view.addButton("view").setValue(1).setPosition(width/1440*14,height/900*175).setSize(width/1440*40,height/900*16);
    view.setAutoDraw(false);
    cp5s[1] = view;

    box = new ControlP5(this);
    box.addButton("box").setValue(2).setPosition(width/1440*20,height/900*286).setSize(width/1440*37,height/900*37);
    box.setAutoDraw(false);
    cp5s[2] = box;

    wheel = new ControlP5(this);
    wheel.addButton("wheel").setValue(3).setPosition(width/1440*21,height/900*362).setSize(width/1440*37,height/900*37);
    wheel.setAutoDraw(false);
    cp5s[3] = wheel;

    drawnline = new ControlP5(this);
    drawnline.addButton("drawnline").setValue(4).setPosition(width/1440*22,height/900*442).setSize(width/1440*37,height/900*37);
    drawnline.setAutoDraw(false);
    cp5s[4] = drawnline;

    drawncurve = new ControlP5(this);
    drawncurve.addButton("drawncurve").setValue(5).setPosition(width/1440*23,height/900*532).setSize(width/1440*37,height/900*37);
    drawncurve.setAutoDraw(false);
    cp5s[5] = drawncurve;

    side = new ControlP5(this);
    side.addButton("side").setValue(6).setPosition(width-width/1440*160,height/900*87).setSize(width/1440*75,height/900*25);
    side.setAutoDraw(false);
    cp5s[6] = side;

    front = new ControlP5(this);
    front.addButton("front").setValue(7).setPosition(width-width/1440*160,height/900*118).setSize(width/1440*75,height/900*25);
    front.setAutoDraw(false);
    cp5s[7] = front;

    rear = new ControlP5(this);
    rear.addButton("rear").setValue(8).setPosition(width-width/1440*160,height/900*149).setSize(width/1440*75,height/900*25);
    rear.setAutoDraw(false);  
    cp5s[8] = rear;

    diagonal = new ControlP5(this);
    diagonal.addButton("diagonal").setValue(9).setPosition(width-width/1440*160,height/900*180).setSize(width/1440*75,height/900*25);
    diagonal.setAutoDraw(false);  
    cp5s[9] = diagonal;  

    bin = new ControlP5(this);
    bin.addButton("bin").setValue(10).setPosition(width/1440*22,height/900*670).setSize(width/1440*37,height/900*37);
    bin.setAutoDraw(false);  
    cp5s[10] = bin;  

    thickness = new ControlP5(this);
    thickness.addButton("thickness").setValue(10).setPosition(width/1440*19, height/900*750).setSize(width/1440*37,height/900*37);
    thickness.setAutoDraw(false);  
    cp5s[11] = thickness;  

    paint = new ControlP5(this);
    paint.addButton("paint").setValue(10).setPosition(width-width/1440*135,height/900*245).setSize(width/1440*20,height/900*20);
    paint.setAutoDraw(false);  
    cp5s[12] = paint;  
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void drawcp5s()
{
    for(ControlP5 c: cp5s){c.draw();}
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void resetWhenToggleView()
{
    if(!(curveNowWorking.isEmpty())&&!(curveNowWorking.get(0).iscomplete()))
    {
        if(sideprofileD.contains(curveNowWorking.get(0))) sideprofileD.remove(curveNowWorking.get(0));
        else if(frontprofileD.contains(curveNowWorking.get(0))) frontprofileD.remove(curveNowWorking.get(0));
        else if(rearprofileD.contains(curveNowWorking.get(0))) rearprofileD.remove(curveNowWorking.get(0));
        else diagonalprofileD.remove(curveNowWorking.get(0));
    }
    
    lineNowWorking.clear(); curveNowWorking.clear();
    workingOnDrawing = false; workingOnResizing = false;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
public void dark()
{
    if(!(buttons.isEmpty())){Button b = buttons.get(0);togglemode(b);} 
}

public void view()
{
    if(!(buttons.isEmpty())){Button b = buttons.get(1);togglemode(b);}
}

public void box()
{
    
    if(!(buttons.isEmpty())){Button b = buttons.get(2);togglemode(b);}
}

public void wheel()
{
    if(!(buttons.isEmpty())){Button b = buttons.get(3);togglemode(b);}
}

public void drawnline()
{
    if(!(buttons.isEmpty())){Button b = buttons.get(4);togglemode(b);}
}

public void drawncurve()
{
    if(!(buttons.isEmpty())){Button b = buttons.get(5);togglemode(b);}
}
public void side()
{
    if(!(buttons.isEmpty())){Button b = buttons.get(6);togglemode(b);resetWhenToggleView();}
}

public void front()
{
    if(!(buttons.isEmpty())){Button b = buttons.get(7);togglemode(b);resetWhenToggleView();}
}

public void rear()
{
    if(!(buttons.isEmpty())){Button b = buttons.get(8);togglemode(b);resetWhenToggleView();}
}
public void diagonal()
{
    if(!(buttons.isEmpty())){Button b = buttons.get(9);togglemode(b);resetWhenToggleView();}
}

public void bin()
{
    if(!(buttons.isEmpty())){Button b = buttons.get(10);togglemode(b);}
}

public void thickness()
{
    if(!(buttons.isEmpty())){Button b = buttons.get(11);togglemode(b);}
}

public void paint()
{
    if(!(buttons.isEmpty())){Button b = buttons.get(12);togglemode(b);}
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////